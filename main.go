package main

import (
	"time"

	"git.workshop21.ch/go/abraxas/logging"
	"git.workshop21.ch/workshop21/ba/operator/configuration"
	"git.workshop21.ch/workshop21/ba/operator/monitoring"
	"git.workshop21.ch/workshop21/ba/operator/monkey"
	"git.workshop21.ch/workshop21/ba/operator/web"
	"github.com/sirupsen/logrus"
)

func init() {
	logrus.SetLevel(logrus.InfoLevel)
}

func main() {

	logging.WithID("PERF-OP-000").Info("operator started")
	config, err := configuration.ReadConfig(nil)
	if err != nil {
		logging.WithID("PERF-OP-1").Fatal(err)
	}
	go web.Serve(config)

	time.Sleep(6 * time.Second)
	//var keys []int
	go monkey.DoTheMonkey(config)
	monitoring.MonitorCluster(config)

}
