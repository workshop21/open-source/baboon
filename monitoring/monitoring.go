package monitoring

import (
	"errors"
	"fmt"
	"strconv"
	"sync"
	"time"

	"git.workshop21.ch/go/abraxas/logging"
	verifier "git.workshop21.ch/workshop21/ba/operator/cluster-verifier"
	"git.workshop21.ch/workshop21/ba/operator/configuration"
	queue "git.workshop21.ch/workshop21/ba/operator/metric-queue"
	"git.workshop21.ch/workshop21/ba/operator/notifier"
	"git.workshop21.ch/workshop21/ba/operator/statistics"
	"git.workshop21.ch/workshop21/ba/operator/util"
)

var datasets map[string]queue.Dataset

var wg sync.WaitGroup

// GetDataset returns a COPY of a dataset
func GetDataset(endpoint string) (queue.Dataset, error) {
	value, ok := datasets[endpoint]
	if ok {
		return value, nil
	}
	return queue.Dataset{Queue: nil}, errors.New("Key not existent!")

}

// GetEndpoints returns a slice of keys as string
func GetEndpoints() []string {
	keys := make([]string, 0, len(datasets))
	for k := range datasets {
		keys = append(keys, k)
	}
	return keys
}

func MonitorCluster(config *configuration.Config) {
	datasets = map[string]queue.Dataset{}
	FillDataset(&datasets, config)
	for _, endpoint := range config.Endpoints {
		logging.WithID("BA-OPERATOR-MONITOR-" + endpoint.Name).Println("generating quantiles")
		statistics.GetQuantiles(datasets[endpoint.Name].Queue.Dataset, config)
	}
	//go VerifyClusterStatusRoutine()
	for {
		wg.Add(len(datasets))
		for _, endpoint := range config.Endpoints {
			now := int(time.Now().Unix())
			go monitorRoutineSecs(datasets[endpoint.Name].Queue, config, endpoint.Path, now, config.SampleInterval)
		}
		wg.Wait()
		VerifyClusterStatus()
		//verifier.VerifyClusterStatus(datasets)
		// why call this function when another already defined function exists in local package?
		//VerifyClusterStatus()
		time.Sleep(time.Duration(config.SampleInterval) * time.Second)
	}
}

// func VerifyClusterStatusRoutine() {
// 	for {

// 		time.Sleep(config.SampleInterval * time.Second)
// 	}
// }

// VerifyClusterStatus return the current cluser status
func VerifyClusterStatus() bool {
	clusterstatus, _, _ := verifyClusterStatus()
	return clusterstatus
}

// verifyClusterStatus evaluates the current cluster status and handles eventual warning and status
func verifyClusterStatus() (bool, []util.StatValues, string) {
	notif := notifier.GetNotifier()
	notification := "Cluster is Healthy"
	status, warning, vals, err := verifier.VerifyClusterStatus(datasets)
	if err != nil {
		logging.WithError("BA-OPERATOR-MONITOR-003", err).Fatalln("not able to determine Cluster state", err)
	}

	handleWarning(warning, vals, notif)

	switch status {
	case util.HEALTHY:
		logging.WithID("BA-OPERATOR-MONITOR-HEALTHY-004").Debug("Cluster is Healthy: ", util.ClusterstateToString(status))
		return true, vals, notification
	case util.DEGRADED:
		notification = fmt.Sprintln("Cluster is Degraded: ", util.ClusterstateToString(status))
		notif.SendStatusNotification(util.StatValuesArrayToString(vals), notification)
		logging.WithID("BA-OPERATOR-MONITOR-DEGRADED-004").Debug(notification)
		return false, vals, notification
	case util.ERROR:
		notification = fmt.Sprintln("Cluster is in Error State!!! : ", util.ClusterstateToString(status))
		notif.SendStatusNotification(util.StatValuesArrayToString(vals), notification)
		logging.WithID("BA-OPERATOR-MONITOR-ERROR-004").Debug(notification)
		return false, vals, notification
	}
	return false, vals, notification

}

// handleWarning handles the eventuality of a warning
func handleWarning(warning int, vals []util.StatValues, notif *notifier.Notifier) {

	switch warning {
	case util.DEGRADED:
		notification := fmt.Sprintln("Cluster is nearly Degraded: ", util.ClusterstateToString(warning))
		notif.SendStatusNotification(util.StatValuesArrayToString(vals), notification)
		logging.WithID("BA-OPERATOR-MONITOR-WARNING-DEGRADED-005").Debug(notification)
	case util.ERROR:
		notification := fmt.Sprintln("Cluster is nearly in Error State: ", util.ClusterstateToString(warning))
		notif.SendStatusNotification(util.StatValuesArrayToString(vals), notification)
		logging.WithID("BA-OPERATOR-MONITOR-WARNING-ERROR-005").Debug(notification)
	}
}

// monitorRoutineSecs read metrics from grafana in a defined interval with parallel
func monitorRoutineSecs(mq *queue.MetricQueue, config *configuration.Config, endpoint string, timeTo int, secs int) {
	defer wg.Done()
	pushDataToQueue(mq, config, endpoint, timeTo, secs)
}

// pushDataToQueue read metrics from grafana in a defined interval
func pushDataToQueue(mq *queue.MetricQueue, config *configuration.Config, endpoint string, timeTo int, secs int) {
	data, err := getMonitoringData(config, endpoint, timeTo, secs)
	if len(data) < 1 || err != nil {
		logging.WithError("MONITOR-ROUTINE-000", err).Error(endpoint, err)
	}
	mq.InsertMonitoringTupelInQueue(data)
	mq.Sort()
}

// MonitorRoutineSecs read metrics from grafana in a defined interval globaly
func MonitorRoutineSecs(mq *queue.MetricQueue, config *configuration.Config, endpoint string, timeTo int, secs int) {
	pushDataToQueue(mq, config, endpoint, timeTo, secs)
}

// FillDataset read initial dataeset of metrics
func FillDataset(datasets *map[string]queue.Dataset, config *configuration.Config) {
	wg.Add(len(config.Endpoints))
	for _, endpoint := range config.Endpoints {

		// go createEndpointDataset(datasets, config, endpoint)
		createEndpointDataset(datasets, config, endpoint)
		time.Sleep(10 * time.Millisecond)
	}
	wg.Wait()
}

// createEndpointDataset create dataset for a define endpoint
func createEndpointDataset(datasets *map[string]queue.Dataset, config *configuration.Config, endpoint configuration.Endpoint) {
	defer wg.Done()
	now := int(time.Now().Unix())

	monQueue := queue.NewMetricQueue()
	pushDataToQueue(monQueue, config, endpoint.Path, now, config.LenghtRecordsToVerify)
	(*datasets)[endpoint.Name] = queue.Dataset{Queue: monQueue, Name: endpoint.Name}
}

// getMonitoringData get the monitoring date for an endpoint
func getMonitoringData(config *configuration.Config, endpoint string, timeStampTo, hoursInPast int) ([]queue.MetricTupel, error) {

	result, err := GetGrafanaResultset(config, endpoint, timeStampTo, hoursInPast)
	if err != nil {
		logging.WithError("PERF-OP-h9u349u43", err).Error(err, endpoint, timeStampTo, hoursInPast)
		return nil, err
	}
	logging.WithID("PERF-OP-0h8943o483f4o8").Debug(result.Status)

	data := make([]queue.MetricTupel, len(result.Data.Result))
	if len(result.Data.Result) < 1 {
		logging.WithID("BA-OPERATOR-GETMONDATA").Error("no data received for", endpoint, "result: ", result.Status)
		return data, errors.New("no data received for" + endpoint + "result: " + result.Status)
	}
	for _, res := range result.Data.Result[0].Values {
		// tm := time.Unix(int64(res[0].(float64)), 0)
		// if err != nil {
		// 	panic(err)
		// }
		value, _ := strconv.ParseFloat(res[1].(string), 64)
		ts := int(res[0].(float64))
		//fmt.Println(ts, "    ", value)
		data = append(data, queue.MetricTupel{Timestamp: ts, Value: value})
		// if value == 0 {
		// 	log.Println(value, endpoint)
		// 	return data
		// }

	}
	return data, err
}
