package monitoring

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"git.workshop21.ch/go/abraxas/logging"
	"git.workshop21.ch/workshop21/ba/operator/configuration"
	"git.workshop21.ch/workshop21/ba/operator/util"
)

// GetGrafanaResultset read metrics from grafana in a defined time
func GetGrafanaResultset(config *configuration.Config, endpoint string, timeStampTo, secondsInPast int) (util.GrafanaResult, error) {
	result := util.GrafanaResult{}
	startTime := timeStampTo - secondsInPast
	stepsize := config.SamplingStepSize
	if strings.Contains(endpoint, "node_network_transmit_bytes") {
		stepsize = "3"
	}
	url := config.MonitoringHost + endpoint + "&start=" + strconv.Itoa(startTime) + "&end=" + strconv.Itoa(timeStampTo) + "&step=" + stepsize
	logging.WithIDFields("PERF-OP-2").Debug(url)
	var bearer = "Bearer " + config.BearerToken
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		logging.WithID("PERF-OP-2.974").Error(err)
		return result, err
	}
	req.Header.Add("authorization", bearer)
	client := http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		logging.WithID("PERF-OP-3").Error(resp, err)
		return result, err
	}
	if resp.StatusCode != 200 {
		logging.WithID("PERF-OP-h97843f7").Error(resp.Status, err)
		return result, errors.New("request failed with error: " + resp.Status + "    " + url)
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &result)
	return result, err
}
