package monitoring

import (
	"errors"
	"strconv"
	"time"

	"git.workshop21.ch/go/abraxas/logging"
	"git.workshop21.ch/workshop21/ba/operator/configuration"
	queue "git.workshop21.ch/workshop21/ba/operator/metric-queue"
	"git.workshop21.ch/workshop21/ba/operator/notifier"
	"git.workshop21.ch/workshop21/ba/operator/statistics"
)

var recoveryqueue *queue.MetricQueue

// RecoveryWatcher Waits for the cluster to fail and then to recover
func RecoveryWatcher() {

	var timeElapsed int64
	config, err := configuration.ReadConfig(nil)
	notifier := notifier.GetNotifier()
	if err != nil {
		logging.WithID("RECOVERY-002").Error(err, err.Error(), "Not able to read Configuration")
		notifier.SendNotification("Not able to read Configuration")
	}
	timeElapsed, err = watchTimeTillClusterRecover(config)
	if err != nil {
		logging.WithID("RECOVERY-003").Error(err, err.Error())
		notifier.SendNotification("Error waiting for the Cluster to recover - " + err.Error())
	} else {
		recoveryqueue = queue.GetQueue(recoveryqueue)
		recoveryqueue.Push(queue.MetricTupel{Timestamp: int(time.Now().Unix()), Value: float64(timeElapsed)})
		logging.WithID("RECOVERY-004").Info("Cluster recovered after ", timeElapsed, " seconds")
	}
}

// run in chan!
func watchTimeTillClusterRecover(config *configuration.Config) (int64, error) {
	for _, endpoint := range config.Endpoints {

		if len(datasets[endpoint.Name].Queue.Dataset) < config.LenghtRecordsToVerify {
			return 0, errors.New("Not enough monitoring records")
		}
	}
	if waitTilClusterFails() {
		start := time.Now().Unix()
		if waitTilClusterRecovers() {
			return time.Now().Unix() - start, nil
		}
		return time.Now().Unix() - start, errors.New("Cluster did not recover in the expected time")

	}
	return 0, errors.New("Cluster did not fail in the expected time")

}

// GetStats returns the 99th, the 90th and the 50th percentile of de recoverytimes recorded during runtime
func GetStats() (string, error) {
	recoveryqueue = queue.GetQueue(recoveryqueue)
	perc99, err := statistics.GetNPercentile(recoveryqueue.Dataset, 0.99)
	if err != nil {
		return err.Error(), err
	}
	perc90, err := statistics.GetNPercentile(recoveryqueue.Dataset, 0.9)
	if err != nil {
		return err.Error(), err
	}
	perc50, err := statistics.GetNPercentile(recoveryqueue.Dataset, 0.5)
	if err != nil {
		return err.Error(), err
	}
	return "perc99: " + strconv.FormatFloat(perc99, 'f', 6, 64) + "\rperc90: " + strconv.FormatFloat(perc90, 'f', 6, 64) + "\rperc50: " + strconv.FormatFloat(perc50, 'f', 6, 64), nil
}

func waitTilClusterFails() bool {
	var counter = 0
	for VerifyClusterStatus() && counter < 300 {
		time.Sleep(1 * time.Second)
		counter++
	}
	return !VerifyClusterStatus()
}

func waitTilClusterRecovers() bool {
	var counter = 0
	for !VerifyClusterStatus() && counter < 300 {
		time.Sleep(1 * time.Second)
		counter++
	}
	return VerifyClusterStatus()
}
