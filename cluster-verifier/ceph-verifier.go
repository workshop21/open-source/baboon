package verifier

import (
	queue "git.workshop21.ch/workshop21/ba/operator/metric-queue"
	stats "git.workshop21.ch/workshop21/ba/operator/statistics"
	"git.workshop21.ch/workshop21/ba/operator/util"
)

// verifyIOPS evaluates the collect IOPS metrics
func verifyIOPS(name string, write *queue.MetricQueue, read *queue.MetricQueue, length int) (util.StatValues, error) {
	writeDS, err := getTupel(name, write, length)
	if len(writeDS) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}
	readDS, err := getTupel(name, read, length)
	if len(writeDS) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	data, err := util.AddTwoMetricTupelArrays(writeDS, readDS)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := avgDevPercForStatus(name, data, 6000.00, 14000.00, 0.75)
	return ret, err
}

// verifyMonitorCounts evaluates the collect monitor metrics
func verifyMonitorCounts(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {
	data, err := getTupel(name, queue, length)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := minForStatus(name, data, 2.00, 2.00)
	return ret, err
}

// verifyOSDCommitLatency evaluates the collected commit latency metrics
func verifyOSDCommitLatency(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {

	commit, err := getTupel(name, queue, length)
	if len(commit) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := avgDevPercForStatus(name, commit, 10.00, 50.00, 0.90)
	return ret, err
}

/* func verifyOSDCommitLatency(queue *queue.MetricQueue, length int) (util.StatValues, error) {

	commit := queue.GetNNewestTupel(length)
	result := stats.Mean(commit, length)
	if len(commit) == 0 {
		return util.GetStatValuesEmpty("ceph-commit"), nil
	}
	deviation := stats.Deviation(commit, length)
	deviation += result

	status := util.HEALTHY
	devStatus := util.HEALTHY
	limitYellow := 10.00
	limitRed := 50.00

	if deviation > limitRed {
		devStatus = util.ERROR
	} else if deviation >= limitYellow && deviation <= limitRed {
		devStatus = util.DEGRADED
	}

	if result > limitRed {
		status = util.ERROR
	} else if result >= limitYellow && result <= limitRed {
		status = util.DEGRADED
	}

	perc90, err := stats.GetNPercentile(commit, 0.90)
	if err != nil {
		return util.GetStatValuesDev("ceph-commit", result, status, deviation, devStatus), nil
	}
	if perc90 > limitRed {
		devStatus = util.ERROR
	} else if perc90 >= limitYellow {
		devStatus = util.DEGRADED
	}

	return util.GetStatValuesAll("ceph-commit", result, status, deviation, devStatus, perc90), nil
} */
// verifyOSDApplyLatency evaluates the collected apply latency metrics
func verifyOSDApplyLatency(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {

	apply, err := getTupel(name, queue, length)
	if len(apply) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := avgDevPercForStatus(name, apply, 10.00, 50.00, 0.90)
	return ret, err
}

/*
func verifyOSDApplyLatency(queue *queue.MetricQueue, length int) (util.StatValues, error) {
	apply := queue.GetNNewestTupel(length)
	if len(apply) == 0 {
		return util.GetStatValuesEmpty("ceph-apply"), nil
	}
	result := stats.Mean(apply, length)
	deviation := stats.Deviation(apply, length)
	deviation += result

	status := util.HEALTHY
	devStatus := util.HEALTHY
	limitYellow := 10.00
	limitRed := 50.00

	if deviation > limitRed {
		devStatus = util.ERROR
	} else if deviation >= limitYellow && deviation <= limitRed {
		devStatus = util.DEGRADED
	}

	if result > limitRed {
		status = util.ERROR
	} else if result >= limitYellow && result <= limitRed {
		status = util.DEGRADED
	}

	perc90, err := stats.GetNPercentile(apply, 0.90)
	if err != nil {
		return util.GetStatValuesDev("ceph-apply", result, status, deviation, devStatus), nil
	}

	if perc90 > limitRed {
		devStatus = util.ERROR
	} else if perc90 >= limitYellow {
		devStatus = util.DEGRADED
	}

	return util.GetStatValuesAll("ceph-apply", result, status, deviation, devStatus, perc90), nil
} */
// verifyCephHealth evaluates the collected CEPH metric for internal health
func verifyCephHealth(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {
	health, err := getTupel(name, queue, length)
	if len(health) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}
	max := stats.Max(health, length)

	res := util.HEALTHY
	if max == 2 {
		res = util.ERROR
	} else if max == 1 {
		res = util.DEGRADED
	}

	return util.GetStatValuesValue(name, max, res), nil
}

/*
func verifyCephHealth(queue *queue.MetricQueue, length int) (util.StatValues, error) {
	health := queue.GetNNewestTupel(length)
	if len(health) == 0 {
		return util.GetStatValuesEmpty("ceph-health"), nil
	}
	max := stats.Max(health, length)

	res := util.HEALTHY
	if max == 2 {
		res = util.ERROR
	} else if max == 1 {
		res = util.DEGRADED
	}

	return util.GetStatValuesValue("ceph-health", max, res), nil
} */
// verifyOSDOrphan evaluates the collected OSD metrics for orphaned OSDs
func verifyOSDOrphan(name string, in *queue.MetricQueue, up *queue.MetricQueue, length int) (util.StatValues, error) {
	inDS, err := getTupel(name, in, length)
	if len(inDS) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}
	upDS, err := getTupel(name, up, length)
	if len(upDS) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	data, err := util.SubTwoMetricTupelArrays(upDS, inDS)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := maxForStatus(name, data, 1.00, 1.00)
	return ret, err
} /*
func verifyOSDOrphan(in *queue.MetricQueue, up *queue.MetricQueue, length int) (util.StatValues, error) {
	inDS := in.GetNNewestTupel(length)
	upDS := up.GetNNewestTupel(length)
	if len(inDS) == 0 || len(upDS) == 0 {
		return util.GetStatValuesEmpty("ceph-orphan"), nil
	}
	data := make([]queue.MetricTupel, length)
	for i := 0; i < length; i++ {
		data[i].Timestamp = upDS[i].Timestamp
		data[i].Value = upDS[i].Value - inDS[i].Value
		if data[i].Value < 0 {
			data[i].Value = 0
		}
	}
	//result := stats.Mean(data, length)
	max := stats.Max(data, length)
	//min := stats.Min(data, length)
	//deviation := stats.Deviation(data, length)

	status := util.HEALTHY
	if max > 1 {
		status = util.ERROR
	} else if max == 1 {
		status = util.DEGRADED
	}

	return util.GetStatValuesValue("ceph-orphan", max, status), nil
} */
// verifyOSDDown evaluates the collected OSD metrics for down OSDs
func verifyOSDDown(name string, up *queue.MetricQueue, in *queue.MetricQueue, length int) (util.StatValues, error) {
	inDS, err := getTupel(name, in, length)
	if len(inDS) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}
	upDS, err := getTupel(name, up, length)
	if len(upDS) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	data, err := util.SubTwoMetricTupelArrays(inDS, upDS)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := maxForStatus(name, data, 1.00, 1.00)
	return ret, err
}

/* func verifyOSDDown(up *queue.MetricQueue, in *queue.MetricQueue, length int) (util.StatValues, error) {
	inDS := in.GetNNewestTupel(length)
	upDS := up.GetNNewestTupel(length)
	if len(inDS) == 0 || len(upDS) == 0 {
		return util.GetStatValuesEmpty("ceph-down"), nil
	}
	data := make([]queue.MetricTupel, length)
	for i := 0; i < length; i++ {
		data[i].Timestamp = upDS[i].Timestamp
		data[i].Value = inDS[i].Value - upDS[i].Value
		if data[i].Value < 0 {
			data[i].Value = 0
		}
	}
	//result := stats.Mean(data, length)
	max := stats.Max(data, length)
	//min := stats.Min(data, length)
	//deviation := stats.Deviation(data, length)
	status := util.HEALTHY
	if max > 1 {
		status = util.ERROR
	} else if max == 1 {
		status = util.DEGRADED
	}

	return util.GetStatValuesValue("ceph-down", max, status), nil
} */

// verifyCapUsage evaluates the collected capacity-usage metrics of ceph
func verifyCapUsage(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {
	usage, err := getTupel(name, queue, length)
	if len(usage) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := avgForStatus(name, usage, 10.00, 80.00)
	return ret, err
}

/* func verifyCapUsage(queue *queue.MetricQueue, length int) (util.StatValues, error) {

	usage := queue.GetNNewestTupel(length)
	if len(usage) == 0 {
		return util.GetStatValuesEmpty("ceph-capacity"), nil
	}
	result := stats.Mean(usage, length)
	//max := stats.Max(usage, length)
	//min := stats.Min(usage, length)
	//deviation := stats.Deviation(usage, length)

	status := util.HEALTHY
	if result > 80 {
		status = util.ERROR
	} else if result >= 10 && result <= 80 {
		status = util.DEGRADED
	}
	return util.GetStatValuesValue("ceph-capacity", result, status), nil
} */

// verifyPG evaluates the collected placementgroup-metrics
func verifyPG(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {
	data, err := getTupel(name, queue, length)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := maxForStatus(name, data, 1.00, 10.00)
	return ret, err
} /*
func verifyPG(queue *queue.MetricQueue, length int, metric string) (util.StatValues, error) {
	usage := queue.GetNNewestTupel(length)
	if len(usage) == 0 {
		return util.GetStatValuesEmpty("ceph-" + metric), nil
	}
	result := stats.Max(usage, length)
	status := util.HEALTHY
	if result > 10 {
		status = util.ERROR
	} else if result >= 1 {
		status = util.DEGRADED
	}
	return util.GetStatValuesValue("ceph-"+metric, result, status), nil
} */
