package verifier

import (
	"fmt"
	"math"

	"time"

	"git.workshop21.ch/go/abraxas/logging"
	"git.workshop21.ch/workshop21/ba/operator/configuration"
	queue "git.workshop21.ch/workshop21/ba/operator/metric-queue"
	stats "git.workshop21.ch/workshop21/ba/operator/statistics"
	"git.workshop21.ch/workshop21/ba/operator/util"
)

var nan = math.NaN()
var config *configuration.Config

// getConfig read the current configuration
func getConfig() (*configuration.Config, error) {
	var err error
	if config == nil {
		config, err = configuration.ReadConfig(config)
		return config, err
	}
	return config, nil
}

// VerifyClusterStatus func cluster status, warning, append(cephdata, infradata...), err
func VerifyClusterStatus(dataset map[string]queue.Dataset) (int, int, []util.StatValues, error) {
	config, err := getConfig()
	statusString := "Healthy"
	if err != nil {
		return 0, 0, nil, err
	}
	length := config.LenghtRecordsToVerify
	logging.WithID("BA-OPERATOR-VERIFIER-01").Info("verifier started")

	var cephdata []util.StatValues
	err = VerifyCephStatus(&cephdata, dataset, length)

	var infradata []util.StatValues
	infraStatus, err := VerfiyInfrastructureStatus(&infradata, dataset, length)

	status := util.HEALTHY
	warning := util.HEALTHY
	for i := 0; i < len(cephdata); i++ {
		if cephdata[i].ValueStatus > status {
			status = cephdata[i].ValueStatus
		}
		// possible because DevStatus is HEALTHY if irrelevant
		if cephdata[i].DevStatus > warning {
			warning = cephdata[i].DevStatus
		}
	}
	if infraStatus == util.ERROR {
		status = util.ERROR
		statusString = "Error"
	} else if status == util.HEALTHY && infraStatus == util.DEGRADED {
		status = util.DEGRADED
		statusString = "Degraded"
	}

	logging.WithID("BA-OPERATOR-VERIFIER-02").Info("Cluster is in state ", statusString)

	return status, warning, append(cephdata, infradata...), err
}

// VerifyCephStatus analyse ceph status
func VerifyCephStatus(struc *[]util.StatValues, dataset map[string]queue.Dataset, length int) error {

	// for every metric an own function, results are collected for the output
	iops, err := verifyIOPS("ceph-iops", dataset["IOPS_write"].Queue, dataset["IOPS_read"].Queue, length)
	*struc = append(*struc, iops)
	logging.WithID("BA-OPERATOR-VERIFIER-08").Info(util.StatValuesToString(iops))

	mon, err := verifyMonitorCounts("ceph-mon", dataset["Mon_quorum"].Queue, length)
	if err != nil {
		logging.WithError("BA-OPERATOR-VERIFIER-08-1", err).Error("Verifying Mon_quorum failed")
	}
	*struc = append(*struc, mon)
	logging.WithID("BA-OPERATOR-VERIFIER-09").Info(util.StatValuesToString(mon))

	commit, err := verifyOSDCommitLatency("ceph-commit", dataset["AvOSDcommlat"].Queue, length)
	*struc = append(*struc, commit)
	logging.WithID("BA-OPERATOR-VERIFIER-10").Info(util.StatValuesToString(commit))

	apply, err := verifyOSDApplyLatency("ceph-apply", dataset["AvOSDappllat"].Queue, length)
	*struc = append(*struc, apply)
	logging.WithID("BA-OPERATOR-VERIFIER-12").Info(util.StatValuesToString(apply))

	health, err := verifyCephHealth("ceph-health", dataset["CEPH_health"].Queue, length)
	*struc = append(*struc, health)
	logging.WithID("BA-OPERATOR-VERIFIER-12").Info(util.StatValuesToString(health))

	orphan, err := verifyOSDOrphan("ceph-orphan", dataset["OSDInQuorum"].Queue, dataset["OSD_UP"].Queue, length)
	*struc = append(*struc, orphan)
	logging.WithID("BA-OPERATOR-VERIFIER-13").Info(util.StatValuesToString(orphan))

	down, err := verifyOSDDown("ceph-down", dataset["OSD_UP"].Queue, dataset["OSDInQuorum"].Queue, length)
	*struc = append(*struc, down)
	logging.WithID("BA-OPERATOR-VERIFIER-17").Info(util.StatValuesToString(down))

	stale, err := verifyPG("ceph-stale", dataset["PG_Stale"].Queue, length)
	*struc = append(*struc, stale)
	logging.WithID("BA-OPERATOR-VERIFIER-18").Info(util.StatValuesToString(stale))

	degraded, err := verifyPG("ceph-degraded", dataset["PG_Degraded"].Queue, length)
	*struc = append(*struc, degraded)
	logging.WithID("BA-OPERATOR-VERIFIER-19").Info(util.StatValuesToString(degraded))

	undersized, err := verifyPG("ceph-undersized", dataset["PG_Undersized"].Queue, length)
	*struc = append(*struc, undersized)
	logging.WithID("BA-OPERATOR-VERIFIER-19").Info(util.StatValuesToString(undersized))

	// tpRead, tpReadStatus, tpReadDev, tpWarning, err := verifyTPRead(dataset["TPread"].Queue, length)
	// data[7] = GetStatValues("throughput read", tpRead, tpReadStatus, tpReadDev, tpWarning)

	// tpWrite, tpWriteStatus, tpWriteDev, tpWarning, err := verifyTPWrite(dataset["TPread"].Queue, length)
	// data[10] = GetStatValues("throughput write", tpWrite, tpWriteStatus, tpWriteDev, tpWarning)

	return err
}

// VerfiyInfrastructureStatus analyse infrastructure status
func VerfiyInfrastructureStatus(struc *[]util.StatValues, dataset map[string]queue.Dataset, length int) (int, error) {
	yellow := 0
	red := 0

	// every metric an own function, results are collected for output
	// status is determined through weighted health definitions
	cpu, err := verifyCPUUsage("cluster-cpu", dataset["PercUsedCPU"].Queue, length)
	logging.WithID("BA-OPERATOR-VERIFIER-03").Info(util.StatValuesToString(cpu))
	*struc = append(*struc, cpu)
	if cpu.ValueStatus == util.DEGRADED {
		yellow += 3
	} else if cpu.ValueStatus == util.ERROR {
		red += 3
	}

	cores, err := verifyCPUCoresUsage("cluster-cores", dataset["CPUCoresUsed"].Queue, length)
	logging.WithID("BA-OPERATOR-VERIFIER-04").Info(util.StatValuesToString(cores))
	*struc = append(*struc, cores)
	if cores.ValueStatus == util.DEGRADED {
		yellow += 3
	} else if cores.ValueStatus == util.ERROR {
		red += 3
	}

	memory, err := verifyMemUsage("cluster-memory", dataset["UsePercOfMem"].Queue, length)
	logging.WithID("BA-OPERATOR-VERIFIER-05").Info(util.StatValuesToString(memory))
	*struc = append(*struc, memory)
	if memory.ValueStatus == util.DEGRADED {
		yellow += 3
	} else if memory.ValueStatus == util.ERROR {
		red += 3
	}

	network, err := verifyNetworkUsage("network", dataset["networkTransmit"].Queue, length)
	logging.WithID("BA-OPERATOR-VERIFIER-06").Info(util.StatValuesToString(network))
	*struc = append(*struc, network)
	if network.ValueStatus == util.DEGRADED {
		yellow += 2
	} else if network.ValueStatus == util.ERROR {
		red += 2
	}

	capacity, err := verifyCapUsage("ceph-capacity", dataset["Av_capacity"].Queue, length)
	logging.WithID("BA-OPERATOR-VERIFIER-07").Info(util.StatValuesToString(capacity))
	*struc = append(*struc, capacity)
	if capacity.ValueStatus == util.DEGRADED {
		yellow++
	} else if capacity.ValueStatus == util.ERROR {
		red++
	}

	daysRemainingCap := predictDaysToCapacitiyLimit(dataset["Av_capacity"].Queue, length)
	logging.WithID("BA-OPERATOR-VERIFIER-15").Info("Predicted Day until Memory: " + util.StatusToStr(daysRemainingCap))

	if red >= 1 {
		return util.ERROR, err
	} else if yellow >= 2 {
		return util.DEGRADED, err
	} else {
		return util.HEALTHY, err
	}
}

// predictDaysToCapacitiyLimit predicts days until capacity of the underlying ceph is not enough with the calculated growth
func predictDaysToCapacitiyLimit(data *queue.MetricQueue, length int) int {
	cap := data.GetNNewestTupel(length)
	timestamp, err := stats.ForecastRegression(cap)
	if err != nil {
		logging.WithError("BA-OPERATOR-VERIFIER-16", err).Error(err)
		return 0
	}
	pred := time.Unix(int64(timestamp), 0)
	diff := time.Until(pred)
	return int(diff.Hours()/24) / 1000
}

func verifyTPRead(queue *queue.MetricQueue, length int) (util.StatValues, error) {
	commit := queue.GetNNewestTupel(length)
	if len(commit) == 0 {
		return util.GetStatValuesEmpty("TPread"), nil
	}
	mean := stats.Mean(commit, length)
	max := stats.Max(commit, length)
	_, perc := stats.GetQuantiles(commit, config)
	fmt.Println(commit, mean, max, perc)

	deviation := stats.Deviation(commit, length)
	deviation += mean

	status := util.HEALTHY
	devStatus := util.HEALTHY
	limitYellow := 10.00
	limitRed := 50.00

	if deviation > limitRed {
		devStatus = util.ERROR
	} else if deviation >= limitYellow && deviation <= limitRed {
		devStatus = util.DEGRADED
	}

	if perc[0.90] > limitRed {
		devStatus = util.ERROR
	} else if perc[0.90] >= limitYellow && perc[0.90] <= limitRed {
		devStatus = util.DEGRADED
	}
	return util.GetStatValuesDev("TPread", perc[0.90], status, deviation, devStatus), nil

}

func verifyTPWrite(queue *queue.MetricQueue, length int) (util.StatValues, error) {
	commit := queue.GetNNewestTupel(length)
	if len(commit) == 0 {
		return util.GetStatValuesEmpty("TPwrite"), nil
	}
	mean := stats.Mean(commit, length)
	max := stats.Max(commit, length)
	_, perc := stats.GetQuantiles(commit, config)
	fmt.Println(commit, mean, max, perc)

	deviation := stats.Deviation(commit, length)
	deviation += mean

	status := util.HEALTHY
	devStatus := util.HEALTHY
	limitYellow := 10.00
	limitRed := 50.00

	if deviation > limitRed {
		devStatus = util.ERROR
	} else if deviation >= limitYellow && deviation <= limitRed {
		devStatus = util.DEGRADED
	}

	if perc[0.90] > limitRed {
		devStatus = util.ERROR
	} else if perc[0.90] >= limitYellow && perc[0.90] <= limitRed {
		devStatus = util.DEGRADED
	}
	return util.GetStatValuesDev("commit", perc[0.90], status, deviation, devStatus), nil

}
