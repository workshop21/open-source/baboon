package verifier

import (
	"errors"

	queue "git.workshop21.ch/workshop21/ba/operator/metric-queue"
	stats "git.workshop21.ch/workshop21/ba/operator/statistics"
	"git.workshop21.ch/workshop21/ba/operator/util"
)

// getTupel returns an Array of MetricTupel with a defined length
func getTupel(name string, queue *queue.MetricQueue, length int) ([]queue.MetricTupel, error) {
	data := queue.GetNNewestTupel(length)
	if len(data) == 0 {
		return data, errors.New("Not able to retain " + name)
	}
	return data, nil
}

// minForStatus uses the statistics min. function to evaluate a status with the defined limits
func minForStatus(name string, data []queue.MetricTupel, limitDeg float64, limitErr float64) (util.StatValues, error) {
	min := stats.Min(data, len(data))
	status := evaluateStatusDescendingValue(min, limitDeg, limitErr)
	return util.GetStatValuesValue(name, min, status), nil
}

// avgDevPercForStatus uses the mean, deviation and percentile functions to evaluate a status and warningstatus with the defined limits
func avgDevPercForStatus(name string, data []queue.MetricTupel, limitDeg float64, limitErr float64, percentage float64) (util.StatValues, error) {
	result := stats.Mean(data, len(data))
	deviation := stats.Deviation(data, len(data)) + result
	perc, _ := stats.GetNPercentile(data, percentage)
	status, warning := evaluateStatusAscendingValueWarning(result, deviation, perc, limitDeg, limitErr)
	return util.GetStatValuesAll(name, result, status, deviation, warning, perc), nil
}

// avgForStatus uses the mean function to evaluate a status with the defined limits
func avgForStatus(name string, data []queue.MetricTupel, limitDeg float64, limitErr float64) (util.StatValues, error) {
	result := stats.Mean(data, len(data))
	status := evaluateStatusAscendingValue(result, limitDeg, limitErr)
	return util.GetStatValuesValue(name, result, status), nil
}

// maxForStatus uses the max. function to evaluate a status with the defined limits
func maxForStatus(name string, data []queue.MetricTupel, limitDeg float64, limitErr float64) (util.StatValues, error) {
	max := stats.Max(data, len(data))
	status := evaluateStatusAscendingValue(max, limitDeg, limitErr)
	return util.GetStatValuesValue(name, max, status), nil
}

// evaluateStatusAscendingValue evaluate a status for an ascending value
func evaluateStatusAscendingValue(value float64, limitDeg float64, limitErr float64) int {
	res := util.HEALTHY
	if value > limitErr {
		res = util.ERROR
	} else if value >= limitErr && value <= limitDeg {
		res = util.DEGRADED
	}
	return res
}

// evaluateStatusAscendingValueWarning evaluate a status and warning for an ascending value
func evaluateStatusAscendingValueWarning(value float64, dev float64, perc float64, limitDeg float64, limitErr float64) (int, int) {
	warning := util.HEALTHY
	if dev > limitErr || perc > limitErr {
		warning = util.ERROR
	} else if (dev >= limitDeg && dev <= limitErr) || (perc >= limitDeg && perc <= limitErr) {
		warning = util.DEGRADED
	}

	status := evaluateStatusAscendingValue(value, limitDeg, limitErr)
	return status, warning
}

// evaluateStatusDescendingValue evaluate a status for an descending value
func evaluateStatusDescendingValue(value float64, limitDeg float64, limitErr float64) int {
	status := util.HEALTHY
	if value < limitErr {
		status = util.ERROR
	} else if value >= limitErr && value <= limitDeg {
		status = util.DEGRADED
	}
	return status
}
