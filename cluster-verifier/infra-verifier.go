package verifier

import (
	queue "git.workshop21.ch/workshop21/ba/operator/metric-queue"
	"git.workshop21.ch/workshop21/ba/operator/util"
)

// verifyCPUUsage evaluates the collected CPU usage metrics
func verifyCPUUsage(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {

	data, err := getTupel(name, queue, length)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := avgDevPercForStatus(name, data, 50.00, 85.00, 0.90)
	return ret, err
} /*
func verifyCPUUsage(queue *queue.MetricQueue, length int) (util.StatValues, error) {

	usage := queue.GetNNewestTupel(length)
	if len(usage) == 0 {
		return util.GetStatValuesEmpty("cpu"), nil
	}
	result := stats.Mean(usage, length)
	//max := stats.Max(usage, length)
	//min := stats.Min(usage, length)
	deviation := stats.Deviation(usage, length)
	status := util.HEALTHY
	devStatus := util.HEALTHY
	if result > 85 {
		status = util.ERROR
	} else if result >= 50 && result <= 85 {
		status = util.DEGRADED
	}
	perc90, err := stats.GetNPercentile(usage, 90)
	if err != nil {
		return util.GetStatValuesValue("cluster-cpu", result, status), nil
	}
	if perc90 > 85 {
		devStatus = util.ERROR
	} else if perc90 >= 50 {
		devStatus = util.DEGRADED
	}
	return util.GetStatValuesAll("cluster-cpu", result, status, deviation, devStatus, perc90), nil
} */

// verifyCPUCoresUsage evaluates the collected CPU-core-usage metrics
func verifyCPUCoresUsage(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {

	data, err := getTupel(name, queue, length)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := avgDevPercForStatus(name, data, 30.00, 50.00, 0.90)
	return ret, err
}

/*
func verifyCPUCoresUsage(queue *queue.MetricQueue, length int) (util.StatValues, error) {

	usage := queue.GetNNewestTupel(length)
	if len(usage) == 0 {
		return util.GetStatValuesEmpty("cluster-cores"), nil
	}
	result := stats.Mean(usage, length)
	//max := stats.Max(usage, length)
	//min := stats.Min(usage, length)
	deviation := stats.Deviation(usage, length)
	status := util.HEALTHY
	devStatus := util.HEALTHY
	if result > 50 {
		status = util.ERROR
	} else if result >= 30 && result <= 50 {
		status = util.DEGRADED
	}
	perc90, err := stats.GetNPercentile(usage, 90)
	if err != nil {
		return util.GetStatValuesValue("cluster-cpu", result, status), nil
	}
	if perc90 > 50 {
		devStatus = util.ERROR
	} else if perc90 >= 30 {
		devStatus = util.DEGRADED
	}
	return util.GetStatValuesAll("cluster-cores", result, status, deviation, devStatus, perc90), nil
} */

// verifyMemUsage evaluates the collected memory-usage metrics
func verifyMemUsage(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {

	data, err := getTupel(name, queue, length)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := avgForStatus(name, data, 60.00, 80.00)
	return ret, err
}

/*
func verifyMemUsage(queue *queue.MetricQueue, length int) (util.StatValues, error) {

	usage := queue.GetNNewestTupel(length)
	if len(usage) == 0 {
		return util.GetStatValuesEmpty("cluster-memory"), nil
	}
	result := stats.Mean(usage, length)
	//max := stats.Max(usage, length)
	//min := stats.Min(usage, length)
	//deviation := stats.Deviation(usage, length)
	status := util.HEALTHY
	if result > 80 {
		status = util.ERROR
	} else if result >= 60 && result <= 80 {
		status = util.DEGRADED
	}
	return util.GetStatValuesValue("cluster-memory", result, status), nil
}
*/

// verifyNetworkUsage evaluates the collected network-usage metrics
func verifyNetworkUsage(name string, queue *queue.MetricQueue, length int) (util.StatValues, error) {

	data, err := getTupel(name, queue, length)
	if len(data) == 0 || err != nil {
		return util.GetStatValuesEmpty(name), err
	}

	ret, err := avgForStatus(name, data, 5000000000.00, 8000000000.00)
	return ret, err
}

/*
func verifyNetworkUsage(transmit *queue.MetricQueue, length int) (util.StatValues, error) {
	data := transmit.GetNNewestTupel(length)
	if len(data) == 0 {
		return util.GetStatValuesEmpty("cluster-network"), nil
	}
	result := stats.Mean(data, length)
	//max := stats.Max(data, length)
	//min := stats.Min(data, length)
	//deviation := stats.Deviation(data, length)

	status := util.HEALTHY
	if result > 8000000000 {
		status = util.ERROR
	} else if result >= 5000000000 && result <= 8000000000 {
		status = util.DEGRADED
	}
	return util.GetStatValuesValue("cluster-network", result, status), nil
}
*/
