package util

import (
	"errors"
	"math"
	"strconv"

	queue "git.workshop21.ch/workshop21/ba/operator/metric-queue"
	"github.com/montanaflynn/stats"
)

func FloatToStr(fv float64) string {
	return strconv.FormatFloat(fv, 'f', 2, 64)
}

func MappingToArray(dataArray []queue.MetricTupel, number int) stats.Float64Data {
	if len(dataArray) < 1 {
		return stats.Float64Data{}
	}
	data := make(stats.Float64Data, number)
	for i := 0; i < number; i++ {
		data[(number-1)-i] = dataArray[len(dataArray)-1-i].Value
	}
	return data
}

func GetStatValuesAll(name string, value float64, valueStatus int, devValue float64, devStatus int, perc float64) StatValues {
	data := StatValues{}
	data.Name = name
	data.Value = value
	data.ValueStatus = valueStatus
	data.DevValue = devValue
	data.DevStatus = devStatus
	data.PercentileVal = perc
	return data
}
func GetStatValuesDev(name string, value float64, valueStatus int, devValue float64, devStatus int) StatValues {
	return GetStatValuesAll(name, value, valueStatus, devValue, devStatus, math.NaN())
}
func GetStatValuesValue(name string, value float64, valueStatus int) StatValues {
	return GetStatValuesDev(name, value, valueStatus, math.NaN(), HEALTHY)
}
func GetStatValuesEmpty(name string) StatValues {
	return GetStatValuesValue(name, math.NaN(), HEALTHY)
}

func ClusterstateToString(status int) string {
	switch status {
	case HEALTHY:
		return "HEALTHY"
	case DEGRADED:
		return "DEGRADED"
	case ERROR:
		return "ERROR"
	}
	return "UNKNOWN"
}

func StatValuesToString(struc StatValues) string {
	ret := struc.Name + ": " + FloatToStr(struc.Value) + " " + StatusToStr(struc.ValueStatus)

	if !math.IsNaN(struc.DevValue) {
		ret = ret + " Deviation: " + FloatToStr(struc.DevValue)
		if !math.IsNaN(struc.PercentileVal) {
			ret = ret + " 90Percentile: " + FloatToStr(struc.PercentileVal)
		}
		ret = ret + " PredictedStatus: " + StatusToStr(struc.DevStatus)
	}
	return ret
}

func StatValuesArrayToString(struc []StatValues) string {
	ret := ""
	for i := 0; i < len(struc); i++ {
		ret += StatValuesToString(struc[i]) + "\n"
	}
	return ret
}

func StatusToStr(stat int) string {
	return ClusterstateToString(stat)
}

func AddTwoMetricTupelArrays(first []queue.MetricTupel, second []queue.MetricTupel) ([]queue.MetricTupel, error) {
	if len(first) != len(second) {
		return nil, errors.New("no matching lengths")
	}

	data := make([]queue.MetricTupel, len(first))
	for i := 0; i < len(first); i++ {
		data[i].Timestamp = first[i].Timestamp
		data[i].Value = first[i].Value + second[i].Value
	}

	return data, nil
}
func SubTwoMetricTupelArrays(first []queue.MetricTupel, second []queue.MetricTupel) ([]queue.MetricTupel, error) {
	if len(first) != len(second) {
		return nil, errors.New("no matching lengths")
	}

	data := make([]queue.MetricTupel, len(first))
	for i := 0; i < len(first); i++ {
		data[i].Timestamp = first[i].Timestamp
		data[i].Value = first[i].Value - second[i].Value
		if data[i].Value < 0 {
			data[i].Value = 0
		}
	}
	return data, nil
}
