package util

const (
	HEALTHY int = 1 + iota
	DEGRADED
	ERROR
)
