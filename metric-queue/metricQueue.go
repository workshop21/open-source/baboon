package queue

import (
	"fmt"
	"sort"
)

var MAX_LENGTH = 3600

type Dataset struct {
	Name  string
	Queue *MetricQueue
}
type MetricTupel struct {
	Timestamp int
	Value     float64
}

type MetricQueue struct {
	Dataset   []MetricTupel
	MaxLength int
}

// GetQueue return pointer of the queue
func GetQueue(q *MetricQueue) *MetricQueue {
	if q == nil {
		return NewMetricQueue()
	} else {
		return q
	}
}

// NewMetricQueue return a newly initialyzed queue
func NewMetricQueue() *MetricQueue {
	return &MetricQueue{Dataset: nil, MaxLength: MAX_LENGTH}
}

// sort define imported sort funciton locally
func (mq *MetricQueue) sort() {
	sort.Sort(mq)
}

// Swap swap two elements in the queue
func (mq *MetricQueue) Swap(i, j int) {
	temp := mq.Dataset[i]
	mq.Dataset[i] = mq.Dataset[j]
	mq.Dataset[j] = temp
}

// Less compare function for sort
func (mq *MetricQueue) Less(i, j int) bool {
	return mq.Dataset[i].Timestamp < mq.Dataset[j].Timestamp
}

// Len return length of the queue
func (mq *MetricQueue) Len() int {
	return len(mq.Dataset)
}

// removeOldestItem remove the oldest element of the queue
func (mq *MetricQueue) removeOldestItem() MetricTupel {
	if len(mq.Dataset) > 0 {
		metricTupeToReturn := mq.Dataset[0]
		mq.Dataset = mq.Dataset[1:]
		return metricTupeToReturn
	}
	return MetricTupel{}

}

// InsertMonitoringTupelInQueue insert array of elements into queue
func (mq *MetricQueue) InsertMonitoringTupelInQueue(tupelArray []MetricTupel) {
	for _, tupel := range tupelArray {
		mq.Push(tupel)
		if len(tupelArray) > mq.MaxLength {
			mq.Pop()
		}

	}
}

// GetNNewestTupel read the N newest elements of the queue
func (mq *MetricQueue) GetNNewestTupel(n int) []MetricTupel {
	if mq == nil || mq.Dataset == nil {
		return make([]MetricTupel, n)
	}
	length := len(mq.Dataset)
	data := make([]MetricTupel, n)
	if !sort.IsSorted(mq) {
		mq.Sort()
	}
	if length > n {
		return mq.Dataset[length-n : length]
	}
	return data
}

// Sort sort the queue comparing the timestamp descending
func (mq *MetricQueue) Sort() {
	if len(mq.Dataset) <= 1 {
		return
	}
	if !sort.IsSorted(mq) {
		mq.sort()
	}
	for {
		if mq.Dataset[0].Timestamp == 0 {
			mq.removeOldestItem()
		} else {
			return
		}
	}
}

// Push append an element to the queue
func (mq *MetricQueue) Push(tupel MetricTupel) {
	mq.Dataset = append(mq.Dataset, tupel)
}

// Pop remove the oldest item of the queue
func (mq *MetricQueue) Pop() MetricTupel {
	return mq.removeOldestItem()
}

// PrintQueue print the queue
func (mq *MetricQueue) PrintQueue() string {
	returnString := ""
	for _, tupel := range mq.Dataset {
		returnString = returnString + fmt.Sprintln(tupel.Timestamp, ": ", tupel.Value)
	}
	fmt.Println(returnString)
	return returnString
}
